# OpenML dataset: Water-Capture-by-Method

https://www.openml.org/d/43583

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
Though it doesn't rain often in Los Angeles, the city has various means of capturing rainfall to increase our local water supply. This dataset shows how much water we've capturing cumulatively this season as well as today. 
Content
Each row in this dataset corresponds to a datetime at which a measurement was made. Measurements include water captured in rain barrels and cisterns, incidental capture, green infrastructure capture, etc. For more details, click the "Data" tab of this dataset.
Methods
This dataset was created using Kaggle's API from this dataset on the City of LA's open data portal:
curl -o los_angeles_water_capture.csv https://data.lacity.org/resource/bnhe-q7a5.csv
kaggle datasets init -p .
kaggle datasets create -p . 
Inspiration

How much does it rain in Los Angeles?
Where does the most rain capture come from?

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43583) of an [OpenML dataset](https://www.openml.org/d/43583). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43583/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43583/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43583/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

